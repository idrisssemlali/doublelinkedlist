
class Node {
    public String data;
    public Node next;
    public Node prev;

    public void displayNodeData() {
        System.out.println(data);
    }
}

public class DoubleLinkedList {

    private Node head;
    private Node tail;
    int size;

   /* public boolean isEmpty() {
        return (head == null);
    }*/


    public void addFront(String data) {
        Node pnoeud = new Node();
        pnoeud.data = data;
        pnoeud.next = head;
        pnoeud.prev=null;
        if(head!=null)
            head.prev=pnoeud;
        head = pnoeud;
        if(tail==null)
            tail=pnoeud;
        size++;
    }


    public void addBack(String data) {
        Node dnoeud = new Node();
        dnoeud.data = data;
        dnoeud.next = null;
        dnoeud.prev=tail;

        if(tail!=null)
            tail.next=dnoeud;
        tail = dnoeud;
        if(head==null)
            head=dnoeud;
        size++;
    }


    public String deteleValue(String value) {

        Node curr = head;
        int conteur = 0;

        // pour savoir si c'est la premiere valeur!!
        if (curr.data == value) {
            conteur = 1;
            head = head.next;
        }
        // checker les autres cases vu que ce n'est pas le premier!!
        else {
            Node curr1 = head;
            while (curr != null) {
                if (curr.data == value) {
                    conteur = 1;
                    break;
                }

                curr1 = curr;
                curr = curr.next;
            }
            if (conteur == 1) {
                System.out.println("La suppression a été faite avec succès");
                curr1.next = curr.next;

            }
        }
        if (conteur == 0) {
            System.out.println("La voiture n'existe pas dans la liste");
        }

        return value;
    }

    public boolean find(String value) {
        //String data;
        Node current = head;
        while (current != null) {
            if (current.data == value){ System.out.println("La voiture " + current.data + " figure dans la liste des voitures");
                return true;
            }
            current = current.next;

        }
        System.out.println("La voiture recherché ne figure pas dans la liste");
        return false;
    }

    public boolean isEmpty() {
        return size == 0;
    }



    public void print() {
        System.out.println("La liste des voitures : ");
        Node current = head;
        while (current != null) {
            current.displayNodeData();
            current = current.next;
        }
    }

}
